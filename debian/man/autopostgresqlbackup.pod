=head1 NAME

autopostgresqlbackup - backup all of your PostgreSQL databases daily, weekly, and monthly

=head1 SYNOPSIS

autopostgresqlbackup

=head1 DESCRIPTION

autopostgresqlbackup is a shell script (usually executed from a cron job) designed
to provide a fully automated tool to make periodic backups of PostgreSQL databases.

On Debian systems, autopostgresqlbackup can be configured by editing some options
in file /etc/default/autopostgresqlbackup.

=head1 AUTHOR

This manpage was written by Emmanuel Bouthenot <kolter@debian.org> for Debian
GNU/Linux but may be used by others.

=cut
